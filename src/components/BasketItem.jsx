
function BasketItem(props) {
    const {
        mainId,
        displayName,
        displayAssets,
        price,
        quantity,
        removeFromBasket
    } = props;

    return (
        <li className="py-3 sm:py-4">
            <div className="flex items-center space-x-4">
                <div className="flex-shrink-0">
                    <img className="w-8 h-8 rounded-full" src={displayAssets[0].background} alt="Neil image"/>
                </div>
                <div className="flex-1 min-w-0">
                    <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                        {quantity} {displayName}
                    </p>
                </div>
                <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    {quantity * price.finalPrice} Руб.
                </div>
                <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                    <span onClick={() => removeFromBasket(mainId)} style={{cursor: "pointer"}}>
                        <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M6 18L18 6M6 6l12 12"></path></svg>
                    </span>
                </div>
            </div>
        </li>
    )
}

export {BasketItem}