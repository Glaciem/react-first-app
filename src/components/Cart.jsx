function Cart(props) {
    const {quantity = 0, handleBasketShow} = props;

    return (
        <div className="cart" onClick={handleBasketShow} data-modal-toggle="medium-modal">
            {quantity ? <span className="cart-quan" style={{fontSize: "1rem"}}><strong>{quantity}</strong></span> : null}
            <svg className="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg>
        </div>
    )
}

export {Cart}