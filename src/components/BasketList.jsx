import {Modal, Button} from "flowbite-react"
import {React} from "react"

import { BasketItem } from "./BasketItem";

function BasketList(props) {

    const {
        isBasktShow,
        handleBasketShow,
        order = [],
        removeFromBasket,
        clearBasket
    } = props;

    const totalPrice = order.reduce((sum, el) => {
        return sum + el.quantity * el.price.finalPrice
    }, 0)

    return (
        <Modal show={isBasktShow} onClose={handleBasketShow} >
            <Modal.Header>
                Корзина
            </Modal.Header>
            <Modal.Body>    
            <div className="flow-root">
                <ul role="list" className="divide-y divide-gray-200 dark:divide-gray-700">
                    <li className="py-3 sm:py-4">
                        <div className="flex items-center space-x-4">
                            {/* <div className="flex-shrink-0">
                                <img className="w-8 h-8 rounded-full" src="/docs/images/people/profile-picture-1.jpg" alt="Neil image"/>
                            </div> */}
                            <div className="flex-1 min-w-0">
                                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                                    <strong>Предметы</strong>
                                </p>
                                <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                                    {/* email@windster.com */}
                                </p>
                            </div>
                            <div className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white">
                                <strong>Цена</strong>
                            </div>
                        </div>
                    </li>
                    {order.map(item => (
                        <BasketItem key={item.mainId} {...item} removeFromBasket={removeFromBasket}/>
                    ))}
                </ul>
            </div>
            </Modal.Body>
            <Modal.Footer style={{justifyContent: "space-between"}}>
                {/* <div className="flex"> */}
                    <Button>
                        Купить
                    </Button>
                    <span style={{fontSize: "1.8rem"}}>
                        {totalPrice} Руб.
                    </span>
                    <Button color="gray" onClick={clearBasket}>
                        Очистить
                    </Button>
                {/* </div> */}
            </Modal.Footer>
        </Modal>
    )
}

export {BasketList}