import {useState, useEffect} from "react"
import {API_KEY, API_URL} from "../config"

import {Prelodaer} from "./Preloader"
import {GoodsList} from "./GoodsList"


function Shop(props) {
    const {addToBasket = Function.prototype , order = []} = props;

    const [goods, setGoods] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        fetch(API_URL, {
            headers: {
                "Authorization": API_KEY
            }
        })
            .then(response => response.json())
            .then((data) => {
                data.shop && setGoods(data.shop);
                setLoading(false);
            });

    }, []);

    return (
        <main className="my-container container flex flex-wrap justify-between items-center mx-auto">
            {loading ? <Prelodaer/> : <GoodsList goods={goods} addToBasket={addToBasket}/>}
        </main>
    )
}

export {Shop}