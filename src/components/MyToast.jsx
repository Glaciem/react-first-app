import {useEffect} from "react"

import {Toast} from "flowbite-react"


function MyToast(props) {
    const { name="", closeAlert = Function.prototype } = props;

    useEffect(() => {
        const timerId = setTimeout(closeAlert, 3000);

        return () => {
            clearTimeout(timerId)
        }
    }, [name])

    return (
        <div className="my-toast">
            <Toast>
                <div className="ml-3 text-sm font-normal">
                    {name} добавлен в коризну!
                </div>
            </Toast>
        </div>
    )
}

export {MyToast}