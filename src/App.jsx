import {useState, useEffect} from "react"

import {Header} from "./components/Header"
import {Footer} from "./components/Footer"
import {Shop} from "./components/Shop"
import {BasketList} from "./components/BasketList"
import { MyToast } from "./components/MyToast"


function setDefaultOrder() {
  const userOrder = JSON.parse(localStorage.getItem("order"));
  return userOrder ? userOrder : [];
}


function App() {

  const [order, setOrder] = useState(setDefaultOrder());
  const [isBasktShow, setBasktShow] = useState(false);
  const [alertName, setAlertName] = useState("");

  const addToBasket = (item) => {
    const itemIndex = order.findIndex(orderItem => orderItem.mainId === item.mainId)
    let newOrder = []
    if (itemIndex < 0) {
      const newItem = {
        ...item,
        quantity: 1,
      }
      newOrder = [...order, newItem]
      setOrder(newOrder);
    } else {
      newOrder = order.map((orderItem, index) => {
        if (index === itemIndex) {
          return {
            ...orderItem,
            quantity: orderItem.quantity +1,
          }
        } else {
          return orderItem
        }
      })

      setOrder(newOrder);
    }
    setAlertName(item.displayName)
  }

  const removeFromBasket = (itemId) => {
    const newOrder = order.filter(el => el.mainId !== itemId);
    setOrder(newOrder);
  }

  const clearBasket = () => {
    setOrder([]);
  }

  const handleBasketShow = () => {
    setBasktShow(!isBasktShow);
  }

  useEffect(function updateLocalStorage() {
    localStorage.setItem("order", JSON.stringify(order));
  }, [order])

  const closeAlert = () => {
    setAlertName("");
  }

  return (
    <>
      <Header order={order} handleBasketShow={handleBasketShow}/>
      {alertName && <MyToast name={alertName} closeAlert={closeAlert}/>}
      <Shop addToBasket={addToBasket}/>
      <Footer />
      <BasketList
        isBasktShow={isBasktShow}
        handleBasketShow={handleBasketShow}
        removeFromBasket={removeFromBasket}
        clearBasket={clearBasket}
        order={order}
      />
    </>
  );
}

export default App;
